import 'package:flutter/material.dart';

class Counterlogic extends ChangeNotifier {
  int count = 0;
  int get _count => count;
  void increment(value) {
    count++;
    notifyListeners();
  }

  void decrement(value) {
    count--;
    notifyListeners();
  }
}
