// ignore: file_names
import 'package:appname/route/router.gr.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../depend/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("MART"),
        centerTitle: true,
      ),
      body: Center(child: Consumer<Counterlogic>(
        builder: (context, value, child) {
          return Column(
            children: [
              Text("${value.count}"),
              ElevatedButton(
                onPressed: () {
                  value.increment(1);
                },
                child: const Text("ADD"),
              ),
              const SizedBox(
                height: 10.0,
              ),
              ElevatedButton(
                onPressed: () {
                  context.router.push(const HomeSecondRoute());
                },
                child: const Text("Next page"),
              ),
            ],
          );
        },
      )),
    );
  }
}
