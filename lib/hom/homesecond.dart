import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../depend/provider.dart';

class HomeSecondPage extends StatelessWidget {
  const HomeSecondPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: SafeArea(
      child: Center(
        child: Consumer<Counterlogic>(
          builder: (context, provider, child) {
            return Column(
              children: [
                Text("${provider.count}"),
                ElevatedButton(
                    onPressed: () {
                      provider.increment(1);
                    },
                    child: const Text("Cart")),
              ],
            );
          },
        ),
      ),
    ));
  }
}
