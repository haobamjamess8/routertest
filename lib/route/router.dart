import 'package:auto_route/annotations.dart';
import '../hom/home.dart';
import '../hom/homesecond.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: HomePage, initial: true),
    AutoRoute(page: HomeSecondPage),
  ],
)
class $AppRouter {}
